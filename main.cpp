long my_Pow(double number, long degree) 
{
	long result = 1;
	while (degree) 
	{
		if (degree % 2 == 0) 
		{
			degree /= 2;
			number *= number;
		}
		else 
		{
			degree--;
			result *= number;
		}
	}
	return result;
}